//
// Created by kuba on 9.11.16.
//

#include "tilt.hpp"
#include "loop.hpp"
#include <ports.h>
#include <unistd.h>

#define SWITCH_MSG(loop, str) do { loop.getMessenger().pushMessage(str); } while (0)

crusher::tilt_cycle::tilt_cycle(ev3dev::address_type const &gyro_port,
                                int threshold,
                                tilt_detection detection,
                                bool color_trig,
                                int color_threshold,
                                display &screen,
                                main_loop &loop)
        : tilt_cycle(loop_phase::line_flat,
                     gyro_port,
                     threshold,
                     detection,
                     color_trig,
                     color_threshold,
                     screen,
                     loop) {}

crusher::tilt_cycle::tilt_cycle(crusher::loop_phase start,
                                ev3dev::address_type const &gyro_port,
                                int threshold,
                                tilt_detection detection,
                                bool color_trig,
                                int color_threshold,
                                display &screen,
                                main_loop &loop)
        : actual(start),
          loop(loop),
          rate(detection == tilt_detection::rate),
          threshold(threshold),
          color_trig(color_trig),
          color_threshold(color_threshold),
          color(color) {

    gyro = get_device_heap_prompt<ev3dev::gyro_sensor>(gyro_port, screen, false, "Gyro sensor");
    usleep(300*1000);
    gyro->set_mode(rate ? gyro->mode_gyro_rate : gyro->mode_gyro_ang);
    usleep(300*1000);
}

crusher::tilt_cycle::~tilt_cycle() {
    delete gyro;
}

crusher::loop_phase crusher::tilt_cycle::update_rate() {
    // positive = CW = up; negative = CCW = down
    int rate = gyro->rate(false);

    // turning downwards
    if (rate < -threshold) {
        switch (actual) {
            case loop_phase::line_flat: SWITCH_MSG(loop, "line down");
                return actual = loop_phase::line_down;
            case loop_phase::line_up:   SWITCH_MSG(loop, "line flat");
                return actual = loop_phase::line_flat;
                // ignore
            case loop_phase::line_down:
            case loop_phase::maze_traversal:
                return actual;
        }
    } // turning upwards
    else if (rate > threshold) {
        switch (actual) {
            case loop_phase::line_down:      SWITCH_MSG(loop, "maze");
                return loop.lineToMaze(), actual = loop_phase::maze_traversal;
            case loop_phase::maze_traversal: SWITCH_MSG(loop, "line up");
                return loop.mazeToLine(), actual = loop_phase::line_up;
                // ignore
            case loop_phase::line_flat:
            case loop_phase::line_up:
                return actual;
        }
    }
    return actual;
}

crusher::loop_phase crusher::tilt_cycle::update_angle() {
    // positive = CW = up; negative = CCW = down
    int angle = gyro->angle(false);

    // moving downwards
    if (angle < -threshold) {
        if (actual != loop_phase::line_down) { SWITCH_MSG(loop, "line down");
            return loop.mazeToLine(), actual = loop_phase::line_down;
        }
    } // moving upwards
    else if (angle > threshold) {
        if (actual != loop_phase::line_up) {   SWITCH_MSG(loop, "line up");
            return actual = loop_phase::line_up;
        }
    } //  moving on flat surface
    else {
        if (actual == loop_phase::line_down) {      SWITCH_MSG(loop, "maze");
            return loop.lineToMaze(), actual = loop_phase::maze_traversal;
        } else if (actual == loop_phase::line_up) { SWITCH_MSG(loop, "line flat");
            return actual = loop_phase::line_flat;
        }
    }
    return actual;
}

crusher::loop_phase crusher::tilt_cycle::update() {
    if (color_trig && actual == loop_phase::maze_traversal) {
        int color = this->color->reflected_light_intensity(false);
        bool paper = color > color_threshold;
        if (paper) {
            SWITCH_MSG(loop, "line up");
            return loop.mazeToLine(), actual = loop_phase::line_up;
        }
    }
    if (touch->is_pressed(false)) {
        if (actual != loop_phase::maze_traversal) {
            loop.lineToMaze();
            loop.control.stop();
            usleep(200*1000);
            gyro->set_mode(rate ? gyro->mode_gyro_rate : gyro->mode_gyro_ang);
            usleep(50*1000);
        }
        return actual = loop_phase::maze_traversal;
    }
    if (rate)
        return update_rate();
    else
        return update_angle();
}
