//
// Created by kuba on 9.11.16.
//

#ifndef CRUSHER_MAP_HPP
#define CRUSHER_MAP_HPP

#include <cstdint>
#include <cstddef>
#include <system_error>

namespace crusher {
    typedef uint_fast8_t tile_int_type;
    enum tile : tile_int_type {

    };

    class map {
        typedef uint_fast8_t tile_type;
        typedef size_t position_type;
    public:
        map() : data(new tile[width * height]) {}

        map(const map &other) : data(new tile[width * height]) {
            std::copy(other.data, other.data + (width * height), data);
        }

        map(map &&other) : data(other.data) {
            other.data = nullptr;
        }

        ~map() {
            if (data != nullptr)
                delete[] data;
        }

        map &operator=(const map &other) {
            std::copy(other.data, other.data + (width * height), this->data);
            return *this;
        }

        map &operator=(map &&other) {
            if (this->data != nullptr)
                delete[] this->data;
            this->data = other.data;
            other.data = nullptr;
            return *this;
        }

        void swap(map &other) {
            map copy(other);
            other = std::move(*this);
            *this = std::move(copy);
        }

        inline tile &at(position_type x, position_type y) {
            return data[arrayIndex(x, y)];
        }

        inline tile const &at(position_type x, position_type y) const {
            return data[arrayIndex(x, y)];
        }

        inline void set(position_type x, position_type y, tile tile) {
            data[arrayIndex(x, y)] = tile;
        }

        constexpr position_type width = 8;
        constexpr position_type height = 9;
    private:
        tile *data;

        inline size_t arrayIndex(position_type x, position_type y) const {
            if (x < 0 || x >= width || y < 0 || y >= height)
                throw std::runtime_error("map access outside bounds");
            return y * width + x;
        }
    };

    void swap(map &a, map &b) {
        a.swap(b);
    }
}


#endif //CRUSHER_MAP_HPP
