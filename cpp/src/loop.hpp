
#ifndef CRUSHER_LOOP_HPP
#define CRUSHER_LOOP_HPP

#include <settings.h>
#include <display.h>
#include <steer_pilot.h>
#include <atomic>
#include <pid_param.h>
#include <pid.h>
#include <queue>
#include <mutex>
#include "mode.hpp"
#include "sensor_arm.hpp"
#include "tilt.hpp"
#include "message_queue.hpp"


//#define DATALOG
//#define DATALOG_SIZE 2048
#define CONCURRENT_MSG


namespace crusher {

    class main_loop {
    public:
        main_loop(robofat::settings &config, robofat::display &disp, line_mode line);

        main_loop(const main_loop &other) = delete;

        ~main_loop();

        main_loop &operator=(const main_loop &other) = delete;

        main_loop &operator=(main_loop &&other) = delete;

        bool line_process(float dt);

        bool wall_process(float dt);

        void prepare();

        void do_work();

        int doSensor(int lightError, float dt);

        int calcRobot(int target, float dt);

        int getSensorOverflow(int destination);

        void setLineParameters();
        void setMazeParameters();

        void mazeToLine();
        void lineToMaze();

        inline bool ended() {
            return end_flag.load();
        }

        inline void terminate() {
            run_flag.store(false);
        }

        inline message_queue &getMessenger() {
            return messenger;
        }
        steer_pilot control;
    private:
        tilt_cycle tiltCycle;
        sensor_arm arm;
        pid pid_sensor;
        pid pid_wheels;
        pid pid_wall;
        const int sensorMin;
        const int sensorMax;
        const int colorTarget;
        const int sensorCenter;
        const float targetCoefficient;
        const float overflowCoefficient;
        const long sleep;


        const int distanceTarget;
        const int mazeTouchTurnConstant;
        const int mazeTouchTravelback;
        const useconds_t mazeTouchTurnWait;
        const useconds_t mazeTouchTravelbackWait;
        const float distanceClip;
        const float freeThreshold;

        const float mazeFreeTurnRatio;
        const int mazeFreeStraightDistance;
        const int mazeFreeTurnDistance;

        settings &config;
        display &screen;
        line_mode lm;
        ev3dev::ultrasonic_sensor *us_sensor;
        ev3dev::touch_sensor *touch_sensor;

        std::atomic_bool end_flag;
        std::atomic_bool run_flag;

        message_queue messenger;
        bool regulators(float diff, loop_phase state);

#ifdef DATALOG
        //////////////////
       // DATA LOGGING //
      //////////////////
          private:
              std::pair<long, short>                    *distanceLog;
              std::pair<long, std::pair<short, short>>  *   colorLog;
#endif
    };
}

#endif //CRUSHER_LOOP_HPP